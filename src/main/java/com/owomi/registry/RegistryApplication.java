package com.owomi.registry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@SpringBootApplication
@EnableEurekaServer
//@PropertySources({
//		@PropertySource(value = "classpath:/owomi-registry.properties", ignoreResourceNotFound = true),
//		@PropertySource(value = "file:${SPRING_CONFIG_LOCATION}/owomi-registry.properties", ignoreResourceNotFound = true),
//		@PropertySource(value = "file:${SPRING_CONFIG_LOCATION}/owomi-registry-${spring.profiles.active}.properties", ignoreResourceNotFound = true)
//})
public class RegistryApplication {

	public static void main(String[] args) {
		SpringApplication.run(RegistryApplication.class, args);
	}

}
