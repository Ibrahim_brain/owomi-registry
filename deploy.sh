export APP_CONFIG_DIRECTORY=$HOME/service-edge-config
mvn clean install -DskipTests
docker rm 127.0.0.1:5000/service_registry
docker build -t 127.0.0.1:5000/service_registry .
docker push 127.0.0.1:5000/service_registry
